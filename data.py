class Data():
    regions =  ["NA", "KR", "LAT", "EU", "ASIA"]

    countries = {
        "NA": ["United States", "Canada"],
        "KR": ["South Korea"],
        "LAT": ["Brazil", "Mexico"],
        "EU": ["Spain", "France"],
        "ASIA": ["China", "Japan"]
    }

    names = {
        "NA": {
            "United States": ["James", "John", "Robert", "Michael", "William", "David", "Richard"],
            "Canada": ["Liam", "Jackson", "Logan", "Lucas", "Noah", "Ethan", "Jack"]
        },
        "KR": {
            "South Korea": ["Do-yun", "Ha-joon", "Seo-jun", "Si-woo", "Min-jun", "Ye-jun", "Ju-won", "Ji-ho"]
        },
        "LAT": {
            "Brazil": ["Joao", "Gabriel", "Pedro", "Lucas", "Matheus", "Guillherme", "Luiz", "Victor"],
            "Mexico": ["Daniel", "Luis", "Alejandro", "Angel", "Carlos", "Diego", "Jorge"]
        },
        "EU": {
            "Spain": ["Santiago", "Matías", "Sebastián", "Mateo", "Nicolás", "Alejandro", "Samuel", "Diego", "Benjamín"],
            "France": ["Gabriel", "Adam", "Raphael", "Paul", "Louis", "Arthur", "Alexandre"]
        },
        "ASIA": {
            "China": ["Bingwen", "Fengge", "Kang", "Liwei", "Enlai", "Guiren", "Chung", "Donghai", "Aiguo"],
            "Japan": ["Haruto", "Riku", "Haru", "Hinata", "Kaito", "Asahi", "Sora", "Reo", "Yuuto"]
        }
    }

    last_names = {
        "NA": {
            "United States": ["Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor", "Anderson", "Thomas", "Jackson", "White", "Harris", "Martin", "Thompson", "Garcia", "Martinez", "Robinson"],
            "Canada": ["Clark", "Rodriguez", "Lewis", "Lee", "Walker", "Hall", "Allen", "Young", "Hernandez", "King", "Wright", "Lopez", "Hill", "Scott", "Green", "Adams", "Baker", "Gonzales", "Nelson", "Carter"],
        },
        "KR": {
            "South Korea": ["Ka", "Kan", "Kal", "Kam", "Kang", "Kyong", "Kye", "Ko", "Kok", "Kong", "Kwak", "Kwan", "Kyo", "Ku", "Kuk", "Kung", "Kim", "Na", "Ra", "Nam", "Nae", "Ta", "Tan", "Tam", "Tang", "Tae", "To", "Ton", "Rah", "Ryu", "Roi", "Ranh", "Ma", "Man", "Min", "Mun", "Muk", "Myo", "Mae", "Pak", "Pal", "Pae", "Bun", "San", "Sub", "Son", "So", "Song", "Sun", "Soi", "Sung", "Ah", "Ya", "Yong", "Ok", "Oh", "O", "Uh", "U", "Won", "Im", "I", "Jang", "Jeon", "Jeom", "Jo", "Jwa", "Jin", "Chang", "Choe", "Choi", "Cho", "Tae", "Ha", "Han", "Ho", "Hwang", "Bae", "Seo"]
        },
        "LAT": {
            "Brazil": ["Abreu", "Acosta", "Afonso", "Agre", "Almada", "Almeida", "Alves", "Arruda", "Avila", "Barbas", "Barbosa", "Barcellos", "Baros", "Bento", "Bras", "Bito", "Cabral", "Cacho", "Caetano", "Cal", "Caldeira", "Camacho", "Camara", "Candido", "Costa", "Cauto", "Da Costa", "Da Cruz", "Da Luz", "Damaso", "De Lima", "De Maura", "Deus", "Dias", "Diniz", "Domingos", "Diniz", "Eanes", "Ferra", "Fidalgo", "Flor", "Fogo", "Fontes", "Formoso", "Frasco", "Furtado", "Garra", "Games", "Gancalves", "Graca", "Guardado", "Henriques", "Hidalgo", "Ines", "Janota", "Joaquim"],
            "Mexico": ["Fernandez", "Rodriguez", "Gonzales", "Lopez", "Romero", "Suarez", "Blanco", "Ruiz", "Sosa", "Iglesias"]
        },
        "EU": {
            "Spain": ["Suarez", "Alonso", "Torres", "Dominguez", "Guitierres", "Hernandez", "Silva", "Otero", "Moreno", "Acosta", "Arias", "Benitez", "Perreyra"],
            "France": ["Abbe", "Acord", "Acy", "Agard", "Aguillon", "Alde", "Bacot", "Badon", "Bailly", "Basile", "Basse", "Baute", "Cardin", "Cailler", "Caron", "Carpentier", "Carre", "Carreau", "Delles"]
        },
        "ASIA": {
            "China": ["Tan", "Lim", "Chua", "Chao", "Ang", "Sison", "Lao", "Tuazon", "Tiu", "Lacson", "Yu", "Dizon", "Wong", "Chan", "Liu", "Zhang", "Larn", "Leung", "Ho", "Wang", "Lee", "Ng", "Goh"],
            "Japan": ["Sato", "Suzuki", "Takahashi", "Tanaka", "Watanabe", "Ito", "Yamamoto", "Nakamura", "Kato"]
        }
    }

    alias_prefix = ["lil", "super", "killer", "another", "saint", "alpha", "food", "hive", "reso", "green", "glass", "smir", "clown", "bord", "boar"]
    alias_suffix = ["xXx", "oWo", "96", "HI", "Bye", "DaDaDa"]