from player import Player
from data import Data
from team import Team

import random
import datetime

data = Data()

def random_date(start, end):
    return start + datetime.timedelta(seconds=random.randint(0, int((end - start).total_seconds())),)

def createPlayer(playerRegion):    
    player = Player

    player.residency = playerRegion
    player.country_of_birth = random.choice(data.countries[player.residency])
    player.bday = random_date(datetime.date(1988,1,1), datetime.date(1999, 11, 20))
    
    player.full_name = random.choice(data.names[player.residency][player.country_of_birth]) + " " + random.choice(data.last_names[player.residency][player.country_of_birth])

    # Mejorar esto. Está horrible :c
    player.alias = random.choice(data.alias_prefix) + player.full_name[:3] +  random.choice(data.alias_suffix)

    print("full_name:", player.full_name)
    print("alias:", player.alias)
    print("residency:", player.residency)
    print("country:", player.country_of_birth)
    print("birthday:", player.bday)
    print()

    return player

def createTeam():
    team_region = random.choice(data.regions)
    
    team = Team
    team.region = team_region
    team.name = "GeneriCTeam"
    
    print("Team name:", team.name)
    print("Team region:", team.region)

    print("Team coach:")
    print()
    team.coach = createPlayer(team_region)


    print("Players from " + team.name)
    print("======")

    for i in range(0,5):
        team.players = []
        team.players.append(createPlayer(team.region))
    
    print("======")

def fight():
    pass

def main():
    for i in range(0,3):
        createTeam()
        print()
        print()

main()